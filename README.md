# 1 STEP 

## Create DB-config (./config/db.php) with next structure

```
<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=127.0.0.1;dbname=test_micro_f1',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```

# 2 Step

## Run this command in console

```
/path/to/php /path/to/yii migrate
```

# 3 Step

## Create params local config(./config/params-local.php) with next structure
```
<?php

return [
    'jwt' => [
        'issuer' => 'http://micro.local',
        'audience' => 'http://micro.local',
        'id' => 'tmsbolirsnbtiolnbtlrlniruw',
    ],
];
```

Also you can rewrite regexp data in this file for parsing apache log file