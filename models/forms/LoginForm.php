<?php


namespace app\models\forms;


use app\models\Users;
use yii\base\Model;

class LoginForm extends Model
{
    public $username;
    public $password;

    /** @var Users|null $_user */
    private $_user;

    public function rules(): array
    {
        return [
            [['username', 'password'], 'required'],
            ['password', 'checkPassword']
        ];
    }

    public function login(array $data = []): array
    {
        $this->setAttributes($data);
        if(!$this->validate()) {
            return [
                'status' => 'ERROR',
                'errors' => $this->errors
            ];
        }

        return [
            'status' => 'OK',
            'data' => [
                'username' => $this->_user->username,
                'token' => $this->_user->generateJWTToken()
            ],
        ];
    }

    public function checkPassword($attribute) :void
    {
        $userModel = Users::findByUsername($this->username);
        if(!\Yii::$app->security->validatePassword($this->password, $userModel->password_hash)) {
            $this->addError($attribute, \Yii::t('app', 'Incorrect password'));
        } else {
            $this->_user = $userModel;
        }
    }
}