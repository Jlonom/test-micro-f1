<?php


namespace app\models\forms;


use app\models\Users;
use yii\base\Model;

class RegisterForm extends Model
{
    public $username;
    public $password;
    public $passwordRepeat;

    public function rules()
    {
        return [
            [['username', 'password', 'passwordRepeat'], 'required'],
            [['password', 'passwordRepeat'], 'string', 'min' => 8],
            ['username', 'unique', 'targetClass' => '\app\models\Users', 'message' => 'This username is use.'],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function register(array $data = [])
    {
        $this->setAttributes($data);
        if(!$this->validate()) {
            return [
                'status' => 'ERROR',
                'errors' => $this->errors
            ];
        }

        $userModel = new Users();
        $userModel->username = $this->username;
        $userModel->password_hash = \Yii::$app->security->generatePasswordHash($this->password);
        if($userModel->save()) {
            return [
                'status' => 'OK',
                'data' => [
                    'username' => $userModel->username,
                    'token' => $userModel->generateJWTToken()
                ],
            ];
        }
    }
}