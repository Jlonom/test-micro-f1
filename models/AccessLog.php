<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "access_log".
 *
 * @property int $id
 * @property string $filepath
 * @property string $ip
 * @property string|null $client_machine
 * @property string|null $client_id
 * @property int $request_time
 * @property string $request_data
 * @property int $response_code
 * @property int $requested_object_size
 * @property string|null $user_agent
 */
class AccessLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'access_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['filepath', 'ip', 'request_time', 'request_data', 'response_code', 'requested_object_size'], 'required'],
            [['request_time', 'response_code', 'requested_object_size'], 'integer'],
            [['request_data'], 'string'],
            [['filepath', 'client_machine', 'client_id', 'user_agent'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'filepath' => Yii::t('app', 'Filepath'),
            'ip' => Yii::t('app', 'Ip'),
            'client_machine' => Yii::t('app', 'Client Machine'),
            'client_id' => Yii::t('app', 'Client ID'),
            'request_time' => Yii::t('app', 'Request Time'),
            'request_data' => Yii::t('app', 'Request Data'),
            'response_code' => Yii::t('app', 'Response Code'),
            'requested_object_size' => Yii::t('app', 'Requested Object Size'),
            'user_agent' => Yii::t('app', 'User Agent'),
        ];
    }
}
