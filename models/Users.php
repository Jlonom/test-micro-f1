<?php

namespace app\models;

use Lcobucci\JWT\Token;
use Yii;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property string|null $auth_key
 * @property string|null $auth_token
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password_hash'], 'required'],
            [['username', 'auth_key', 'auth_token'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'max' => 64],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'auth_token' => Yii::t('app', 'Auth Token'),
        ];
    }

    public function generateJWTToken()
    {
        $jwt = Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();

        return (string) $jwt->getBuilder()
            ->issuedBy('http://example.com')// Configures the issuer (iss claim)
            ->permittedFor('http://example.org')// Configures the audience (aud claim)
            ->identifiedBy('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
            ->issuedAt($time)// Configures the time that the token was issue (iat claim)
            ->expiresAt($time + 3600)// Configures the expiration time of the token (exp claim)
            ->withClaim('uid', $this->id)// Configures a new claim, called "uid"
            ->getToken($signer, $key);
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @param Token $token
     * @param null $type
     * @return void|IdentityInterface|null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = self::findOne((string)$token->getClaim('uid'));
        if($user !== null) {
            return $user;
        }

        throw new NotFoundHttpException(Yii::t('app', 'User not found'));
    }

    /**
     * @param string $username
     * @return array|self|null
     * @throws NotFoundHttpException
     */
    public static function findByUsername(string $username)
    {
        if(($model = self::find()->where(['=', 'username', $username])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'User not found'));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}
