<?php


namespace app\components;


class JwtValidationData extends \sizeg\jwt\JwtValidationData
{
    public function init()
    {
        $this->validationData->setIssuer(\Yii::$app->params['jwt']['issuer']);
        $this->validationData->setAudience(\Yii::$app->params['jwt']['audience']);
        $this->validationData->setId(\Yii::$app->params['jwt']['id']);

        parent::init();
    }
}