<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'jwt' => [
        'issuer' => 'http://example.com',
        'audience' => 'http://example.com',
        'id' => '4f1g23a12aa',
    ],
    'access_log' => [
        'filepath' => __DIR__ . '/../access.log',
        'regexp' => '#(.*?)\s(.*?)\s(.*?)\s\[(.*?)\]\s\"(.*?)\"\s(\d{3})\s(\d{1,})\s(.*?)\s\"(.*?)\"\s\"(.*?)\"(\n?)#si',
        'mapping' => [
            'ip' => 1,
            'client_machine' => 2,
            'client_id' => 3,
            'request_time' => 4,
            'request_data' => 5,
            'response_code' => 6,
            'requested_object_size' => 7,
            'user_agent' => 9
        ],
    ],
];
