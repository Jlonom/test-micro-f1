<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%access_log}}`.
 */
class m210217_163013_create_access_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%access_log}}', [
            'id' => $this->primaryKey(),
            'filepath' => $this->string(255)->notNull(),
            'ip' => $this->string(15)->notNull(),
            'client_machine' => $this->string(255)->null(),
            'client_id' => $this->string(255)->null(),
            'request_time' => $this->integer()->notNull(),
            'request_data' => $this->text()->notNull(),
            'response_code' => $this->integer()->notNull(),
            'requested_object_size' => $this->integer()->notNull(),
            'user_agent' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%access_log}}');
    }
}
