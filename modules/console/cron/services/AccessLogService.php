<?php


namespace app\modules\console\cron\services;


use yii\console\Exception;
use yii\db\Expression;
use yii\db\Query;

class AccessLogService
{
    public $logfile;
    public $records;
    public $attributeNames;

    public function __construct($filepath)
    {
        if(empty($filepath)) {
            throw new Exception(\Yii::t('app', 'File not exists'));
        }

        if(!is_readable($filepath)) {
            throw new Exception(\Yii::t('app', 'You haven\'t access to this file!'));
        }

        $this->logfile = $filepath;
    }

    public function importNewRecords() :int
    {
        $lastSavedRecordTime = $this->getLastSavedRecordTime();
        $this->importRecordsAfterLast($lastSavedRecordTime);
        $insertedCount = $this->insertImportingRecords();
        return $insertedCount;
    }

    protected function getLastSavedRecordTime()
    {
        return (int)(new Query())
            ->select([new Expression('MAX(request_time)')])
            ->from('{{%access_log}}')
            ->where(['=', 'filepath', $this->logfile])
            ->scalar();
    }

    protected function importRecordsAfterLast(int $lastRecordTime)
    {
        $this->prepareAttributeNamesForInsert();
        $handle = fopen($this->logfile, "r");
        while(!feof($handle) && ($line = fgets($handle)) !== false) {
            $regexp = \Yii::$app->params['access_log']['regexp'];
            preg_match_all($regexp, $line, $matches);
            $this->prepareRowToBatchInsert($matches);
        }
    }

    protected function prepareAttributeNamesForInsert()
    {
        foreach(\Yii::$app->params['access_log']['mapping'] as $attributeName => $index) {
            if ($index !== null) {
                $this->attributeNames[] = $attributeName;
            }
        }
        $this->attributeNames[] = 'filepath';
    }

    protected function prepareRowToBatchInsert(array $matchesParts)
    {
        $row = [];
        foreach(\Yii::$app->params['access_log']['mapping'] as $attributeName => $index) {
            if ($index !== null) {
                $row[] = $matchesParts[$index][0];
            }
        }
        $row[] = $this->logfile;
        $this->records[] = $row;
    }

    protected function insertImportingRecords()
    {
        return (new Query())
            ->createCommand()
            ->batchInsert('{{%access_log}}', $this->attributeNames, $this->records)
            ->execute();
    }
}