<?php


namespace app\modules\console\cron\controllers;


use app\modules\console\cron\services\AccessLogService;
use yii\console\Controller;
use yii\console\Exception;

class ApacheParseController extends Controller
{
    public function actionIndex()
    {
        $filePath = realpath(\Yii::$app->params['access_log']['filepath']);
        try {
            $accessLogService = new AccessLogService($filePath);
            $count = $accessLogService->importNewRecords();
            $this->stdout(\Yii::t('app', 'Importing {count} new records', ['count' => $count]) . "\n");
        } catch (Exception $e) {
            $this->stderr($e->getMessage());
        }
    }
}