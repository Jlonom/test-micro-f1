<?php


namespace app\modules\frontend\api\actions\users;

use app\models\forms\RegisterForm;
use yii\helpers\Json;

class RegisterAction extends \yii\rest\CreateAction
{
    public function run()
    {
        $requestData = Json::decode(\Yii::$app->request->getRawBody());
        $response = (new RegisterForm)->register($requestData);
        if($response['status'] === 'ERROR') {
            \Yii::$app->response->statusCode = 400;
        }
        return $response;
    }
}