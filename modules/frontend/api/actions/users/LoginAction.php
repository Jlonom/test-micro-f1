<?php


namespace app\modules\frontend\api\actions\users;


use app\models\forms\LoginForm;
use yii\helpers\Json;
use yii\rest\Action;

class LoginAction extends Action
{
    public function run(): array
    {
        $requestData = Json::decode(\Yii::$app->request->getRawBody());
        $response = (new LoginForm())->login($requestData);
        if($response['status'] === 'ERROR') {
            \Yii::$app->response->statusCode = 400;
        }
        return $response;
    }
}