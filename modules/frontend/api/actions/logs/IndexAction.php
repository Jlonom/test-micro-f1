<?php


namespace app\modules\frontend\api\actions\logs;


use app\modules\frontend\api\services\AccessLogService;
use yii\rest\Action;

class IndexAction extends Action
{

    public function run(
        ?string $timeFrom,
        ?string $timeTo,
        ?int $itemsPerPage = null,
        ?int $currentPage = null,
        ?array $ips = []
    )
    {
        $accessService = new AccessLogService($timeFrom, $timeTo, $itemsPerPage, $currentPage, $ips);
        return $accessService->getRecords();
    }
}