<?php


namespace app\modules\frontend\api\controllers;

use app\models\Users;
use yii\filters\AccessControl;
use yii\rest\ActiveController;

class UsersController extends ActiveController
{
    public $modelClass = Users::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter' ] = [
            'class' => \yii\filters\Cors::className(),
        ];
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['create', 'update'],
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['create', 'update'],
                    'roles' => ['?']
                ]
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['view'], $actions['update'], $actions['index'], $actions['delete'], $actions['options']);

        // Register action
        $actions['create'] = [
            'class' => 'app\modules\frontend\api\actions\users\RegisterAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        // Login action
        $actions['login'] = [
            'class' => 'app\modules\frontend\api\actions\users\LoginAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }
}