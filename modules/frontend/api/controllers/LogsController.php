<?php


namespace app\modules\frontend\api\controllers;


use app\models\AccessLog;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\filters\AccessControl;
use yii\rest\ActiveController;

class LogsController extends ActiveController
{
    public $modelClass = AccessLog::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter' ] = [
            'class' => \yii\filters\Cors::className(),
        ];
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['index'],
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['index'],
                    'roles' => ['@']
                ]
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['view'], $actions['update'], $actions['create'], $actions['delete'], $actions['options']);

        $actions['index'] = [
            'class' => 'app\modules\frontend\api\actions\logs\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }
}