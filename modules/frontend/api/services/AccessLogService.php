<?php


namespace app\modules\frontend\api\services;


use yii\db\Query;

class AccessLogService
{
    const DEFAULT_ITEMS_PER_PAGE = 20;
    const DEFAULT_PAGE = 1;

    protected $timeFrom;
    protected $timeTo;
    protected $itemsPerPage;
    protected $currentPage;
    protected $ips;

    public function __construct(
        ?string $timeFrom,
        ?string $timeTo,
        ?int $itemsPerPage = self::DEFAULT_ITEMS_PER_PAGE,
        ?int $currentPage = 1,
        ?array $ips = []
    )
    {
        $this->timeFrom = $timeFrom;
        $this->timeTo = $timeTo;

        if($itemsPerPage < 1) {
            $itemsPerPage = self::DEFAULT_ITEMS_PER_PAGE;
        }
        $this->itemsPerPage = $itemsPerPage;

        if($currentPage < 1) {
            $currentPage = self::DEFAULT_PAGE;
        }
        $this->currentPage = $currentPage;

        if(!is_array($ips) && $ips !== null) {
            $ips = [$ips];
        }
        $this->ips = $ips;
    }

    public function getRecords()
    {
        $query = (new Query())
            ->select(['*'])
            ->from('{{%access_log}}')
            ->limit($this->itemsPerPage)
            ->offset(($this->currentPage - 1) * $this->itemsPerPage);

        if($this->timeFrom !== null) {
            $query->andWhere(['>', 'request_time', strtotime($this->timeFrom)]);
        }

        if($this->timeTo !== null) {
            $query->andWhere(['<', 'request_time', strtotime($this->timeTo)]);
        }

        if(!empty($this->ips)) {
            $query->andWhere(['IN', 'ip', $this->ips]);
        }

        $countQuery = clone $query;

        return [
            'currentPage' => $this->currentPage,
            'itemsPerPage' => $this->itemsPerPage,
            'totalItems' => $countQuery->limit(null)->offset(null)->count(),
            'items' => $query->all()
        ];
    }
}